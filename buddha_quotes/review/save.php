<?php


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$lang = $conn -> real_escape_string($_GET['lang']);

$sql = "UPDATE bandev_bq_languages SET ready=1 WHERE lang_iso='$lang'";

if ($conn->query($sql) === TRUE) {
  header('location: done');
} else {
  echo "Error updating record: " . $conn->error;
}

$conn->close();

?>