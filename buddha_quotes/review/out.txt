<?xml version='1.0' encoding='utf-8'?>

<!-- Translated to fr at 12:15:55pm on 2020-10-27-->

<resources>

    <string name='refresh'>Rafraîchir</string> <!-- Translated by jack.txt -->
    <string name='favourite'>Préférée</string> <!-- Translated by jack.txt -->
    <string name='share'>partager</string> <!-- Translated by jack.txt -->
    <string name='settings'>réglages</string> <!-- Translated by jack.txt -->
    <string name='appearance'>apparence</string> <!-- Translated by jack.txt -->
    <string name='app_theme'>thème de l'application</string> <!-- Translated by harrydevey06 -->
    <string name='light_mode'>lumière</string> <!-- Translated by harrydevey06 -->
    <string name='dark_mode'>sombre</string> <!-- Translated by harrydevey06 -->
    <string name='follow_system_default'>Suivre les paramètres par défaut du système</string> <!-- Translated by harrydevey06 -->
    <string name='text_size'>Taille du texte</string> <!-- Translated by harrydevey06 -->
    <string name='small_text_size'>Petit</string> <!-- Translated by harrydevey06 -->
    <string name='medium_text_size'>moyen</string> <!-- Translated by harrydevey06 -->
    <string name='large_text_size'>Grand</string> <!-- Translated by harrydevey06 -->
    <string name='information'>information</string> <!-- Translated by harrydevey06 -->
    <string name='about'>sur</string> <!-- Translated by harrydevey06 -->
    <string name='view_app_details'>Afficher les détails de l'application</string> <!-- Translated by harrydevey06 -->
    <string name='help'>Aidez-moi</string> <!-- Translated by harrydevey06 -->
    <string name='app_introduction'>introduction de l'application</string> <!-- Translated by harrydevey06 -->
    <string name='app_license'>Licence d'application</string> <!-- Translated by harrydevey06 -->
    <string name='libraries_used'>Bibliothèques utilisées</string> <!-- Translated by harrydevey06 -->
    <string name='about_app_promise'>Promesse d'application</string> <!-- Translated by harrydevey06 -->
    <string name='about_no_trackers'>Pas de trackers</string> <!-- Translated by harrydevey06 -->
    <string name='about_complete_privacy'>Confidentialité totale</string> <!-- Translated by harrydevey06 -->
    <string name='about_offline_app'>Application hors ligne</string> <!-- Translated by harrydevey06 -->
    <string name='about_free_and_open_source'>Gratuit et Open Source</string> <!-- Translated by harrydevey06 -->
    <string name='about_info'>Informations sur l'application</string> <!-- Translated by harrydevey06 -->
    <string name='about_info_paragraph'>Buddha Quotes est un projet collaboratif pour créer une application gratuite et open source Buddha Quotes pour Android avec un accent sur la confidentialité</string> <!-- Translated by harrydevey06 -->
    <string name='about_contributors'>Contributeurs</string> <!-- Translated by harrydevey06 -->
    <string name='about_acknowledgements'>Remerciements</string> <!-- Translated by harrydevey06 -->
    <string name='about_attribution'>Icône du lanceur - Freepik via Flaticon</string> <!-- Translated by harrydevey06 -->
    <string name='tick'>Cocher</string> <!-- Translated by harrydevey06 -->
    <string name='quick_brown_fox'>Le renard brun rapide saute par-dessus le chien paresseux</string> <!-- Translated by harrydevey06 -->
    <string name='slides_continue'>Continuer</string> <!-- Translated by harrydevey06 -->
    <string name='slide_1_free_libre_open_source'>Gratuit, libre et open source</string> <!-- Translated by harrydevey06 -->
    <string name='slide_2_privacy_focused'>Axé sur la confidentialité!</string> <!-- Translated by harrydevey06 -->
    <string name='slide_2_bandev_promise'>Avec la promesse BanDev</string> <!-- Translated by harrydevey06 -->
    <string name='slide_2_lock'>fermer à clé</string> <!-- Translated by harrydevey06 -->
    <string name='slide_3_see_one_you_like'>Vous en voyez un que vous aimez?</string> <!-- Translated by harrydevey06 -->
    <string name='slide_3_press_heart_button'>Appuyez sur le bouton du cœur</string> <!-- Translated by harrydevey06 -->
    <string name='slide_3_heart_outline'>Contour de coeur</string> <!-- Translated by harrydevey06 -->
    <string name='slide_4_another_quote'>Une autre citation?</string> <!-- Translated by harrydevey06 -->
    <string name='slide_4_hit_refresh'>Appuyez simplement sur Actualiser</string> <!-- Translated by harrydevey06 -->
    <string name='slide_5_too_bright'>Affichage trop clair?</string> <!-- Translated by harrydevey06 -->
    <string name='slide_5_try_dark'>Pourquoi ne pas essayer le mode sombre</string> <!-- Translated by harrydevey06 -->
    <string name='slide_5_torch'>Torche</string> <!-- Translated by harrydevey06 -->
    <string name='slide_6_hard_to_read'>Texte difficile à lire?</string> <!-- Translated by harrydevey06 -->
    <string name='slide_6_select_size'>Sélectionnez une taille de texte ci-dessous</string> <!-- Translated by harrydevey06 -->
    <string name='slide_7_all_done'>Terminé!</string> <!-- Translated by harrydevey06 -->
    <string name='slide_7_ready'>tu</string> <!-- Translated by harrydevey06 -->
    <string name='slide_7_open_app'>Ouvrir application</string> <!-- Translated by harrydevey06 -->
    <string name='slide_7_double_tick'>Double tick</string> <!-- Translated by harrydevey06 -->
    <string name='more_counter_left'>Une autre citation</string> <!-- Translated by harrydevey06 -->
    <string name='more_lots_of_refreshes'>Ce</string> <!-- Translated by harrydevey06 -->
    <string name='favourites'>Favoris</string> <!-- Translated by harrydevey06 -->
    <string name='favourited_quote'>Citation préférée</string> <!-- Translated by harrydevey06 -->
    <string name='favourite_quote_delete'>Effacer</string> <!-- Translated by harrydevey06 -->
    <string name='buddha_quotes_logo'>Logo de citations de Bouddha</string> <!-- Translated by harrydevey06 -->
    <string name='bandev_logo'>Logo BanDev</string> <!-- Translated by harrydevey06 -->
    <string name='the_buddha'>Le Bouddha</string> <!-- Translated by harrydevey06 -->
    <string name='forward_arrow'>Revenir</string> <!-- Translated by harrydevey06 -->
    <string name='icon'>Icône</string> <!-- Translated by harrydevey06 -->
    <string name='title'>Titre</string> <!-- Translated by harrydevey06 -->
    <string name='licenses'>Licences</string> <!-- Translated by harrydevey06 -->
    <string name='licenses_permissions'>Autorisations</string> <!-- Translated by harrydevey06 -->
    <string name='licenses_commercial_use'>Un usage commercial</string> <!-- Translated by harrydevey06 -->
    <string name='licenses_distribution'>Distribution</string> <!-- Translated by harrydevey06 -->
    <string name='licenses_modification'>Modification</string> <!-- Translated by harrydevey06 -->
    <string name='licenses_patent_use'>Utilisation des brevets</string> <!-- Translated by harrydevey06 -->
    <string name='licenses_private_use'>Usage privé</string> <!-- Translated by jack.txt -->
    <string name='licenses_conditions'>Conditions</string> <!-- Translated by jack.txt -->
    <string name='licenses_disclose_source'>Divulguer la source</string> <!-- Translated by jack.txt -->
    <string name='licenses_copyright_notice'>Avis de licence et de copyright</string> <!-- Translated by jack.txt -->
    <string name='licenses_same_license'>Même licence</string> <!-- Translated by jack.txt -->
    <string name='licenses_state_changes'>Changements d'état</string> <!-- Translated by jack.txt -->
    <string name='licenses_limitations'>Limites</string> <!-- Translated by jack.txt -->
    <string name='licenses_liability'>Responsabilité</string> <!-- Translated by jack.txt -->
    <string name='licenses_warranty'>garantie</string> <!-- Translated by jack.txt -->
    <string name='licenses_full_license'>Licence complète</string> <!-- Translated by jack.txt -->
    <string name='green_circle'>Cercle vert</string> <!-- Translated by jack.txt -->
    <string name='blue_circle'>Cercle bleu</string> <!-- Translated by jack.txt -->
    <string name='red_circle'>Cercle rouge</string> <!-- Translated by jack.txt -->
    <string name='title_activity_settings'>ParamètresActivité</string> <!-- Translated by jack.txt -->
    <string name='textview'>Affichage</string> <!-- Translated by jack.txt -->
    <string name='MainActivity'>Activité principale</string> <!-- Translated by jack.txt -->
    <string name='title_activity_scrolling'>ScrollingActivity</string> <!-- Translated by jack.txt -->
    <string name='action_settings'>Réglages</string> <!-- Translated by jack.txt -->
    <string name='appwidget_text'>EXEMPLE</string> <!-- Translated by jack.txt -->
    <string name='configure'>Configurer</string> <!-- Translated by jack.txt -->
    <string name='add_widget'>Ajouter un widget</string> <!-- Translated by jack.txt -->
    <string name='open_source_libraries'>Bibliothèques Open Source</string> <!-- Translated by jack.txt -->
    <string name='about_no_ads'>Pas de pubs</string> <!-- Translated by jack.txt -->


<resources>