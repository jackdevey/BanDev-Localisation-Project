<?php
session_start();

if(empty($_SESSION['user'])){
    header('location: ../');
}
session_start();
$uname = $_SESSION['user']['username'];

$codes = [
    'ab' => 'Abkhazian',
    'aa' => 'Afar',
    'af' => 'Afrikaans',
    'ak' => 'Akan',
    'sq' => 'Albanian',
    'am' => 'Amharic',
    'ar' => 'Arabic',
    'an' => 'Aragonese',
    'hy' => 'Armenian',
    'as' => 'Assamese',
    'av' => 'Avaric',
    'ae' => 'Avestan',
    'ay' => 'Aymara',
    'az' => 'Azerbaijani',
    'bm' => 'Bambara',
    'ba' => 'Bashkir',
    'eu' => 'Basque',
    'be' => 'Belarusian',
    'bn' => 'Bengali',
    'bh' => 'Bihari languages',
    'bi' => 'Bislama',
    'bs' => 'Bosnian',
    'br' => 'Breton',
    'bg' => 'Bulgarian',
    'my' => 'Burmese',
    'ca' => 'Catalan, Valencian',
    'km' => 'Central Khmer',
    'ch' => 'Chamorro',
    'ce' => 'Chechen',
    'ny' => 'Chichewa, Chewa, Nyanja',
    'zh' => 'Chinese',
    'cu' => 'Church Slavonic, Old Bulgarian, Old Church Slavonic',
    'cv' => 'Chuvash',
    'kw' => 'Cornish',
    'co' => 'Corsican',
    'cr' => 'Cree',
    'hr' => 'Croatian',
    'cs' => 'Czech',
    'da' => 'Danish',
    'dv' => 'Divehi, Dhivehi, Maldivian',
    'nl' => 'Dutch, Flemish',
    'dz' => 'Dzongkha',
    'en' => 'English',
    'eo' => 'Esperanto',
    'et' => 'Estonian',
    'ee' => 'Ewe',
    'fo' => 'Faroese',
    'fj' => 'Fijian',
    'fi' => 'Finnish',
    'fr' => 'French',
    'ff' => 'Fulah',
    'gd' => 'Gaelic, Scottish Gaelic',
    'gl' => 'Galician',
    'lg' => 'Ganda',
    'ka' => 'Georgian',
    'de' => 'German',
    'ki' => 'Gikuyu, Kikuyu',
    'el' => 'Greek (Modern)',
    'kl' => 'Greenlandic, Kalaallisut',
    'gn' => 'Guarani',
    'gu' => 'Gujarati',
    'ht' => 'Haitian, Haitian Creole',
    'ha' => 'Hausa',
    'he' => 'Hebrew',
    'hz' => 'Herero',
    'hi' => 'Hindi',
    'ho' => 'Hiri Motu',
    'hu' => 'Hungarian',
    'is' => 'Icelandic',
    'io' => 'Ido',
    'ig' => 'Igbo',
    'id' => 'Indonesian',
    'ia' => 'Interlingua (International Auxiliary Language Association)',
    'ie' => 'Interlingue',
    'iu' => 'Inuktitut',
    'ik' => 'Inupiaq',
    'ga' => 'Irish',
    'it' => 'Italian',
    'ja' => 'Japanese',
    'jv' => 'Javanese',
    'kn' => 'Kannada',
    'kr' => 'Kanuri',
    'ks' => 'Kashmiri',
    'kk' => 'Kazakh',
    'rw' => 'Kinyarwanda',
    'kv' => 'Komi',
    'kg' => 'Kongo',
    'ko' => 'Korean',
    'kj' => 'Kwanyama, Kuanyama',
    'ku' => 'Kurdish',
    'ky' => 'Kyrgyz',
    'lo' => 'Lao',
    'la' => 'Latin',
    'lv' => 'Latvian',
    'lb' => 'Letzeburgesch, Luxembourgish',
    'li' => 'Limburgish, Limburgan, Limburger',
    'ln' => 'Lingala',
    'lt' => 'Lithuanian',
    'lu' => 'Luba-Katanga',
    'mk' => 'Macedonian',
    'mg' => 'Malagasy',
    'ms' => 'Malay',
    'ml' => 'Malayalam',
    'mt' => 'Maltese',
    'gv' => 'Manx',
    'mi' => 'Maori',
    'mr' => 'Marathi',
    'mh' => 'Marshallese',
    'ro' => 'Moldovan, Moldavian, Romanian',
    'mn' => 'Mongolian',
    'na' => 'Nauru',
    'nv' => 'Navajo, Navaho',
    'nd' => 'Northern Ndebele',
    'ng' => 'Ndonga',
    'ne' => 'Nepali',
    'se' => 'Northern Sami',
    'no' => 'Norwegian',
    'nb' => 'Norwegian Bokmål',
    'nn' => 'Norwegian Nynorsk',
    'ii' => 'Nuosu, Sichuan Yi',
    'oc' => 'Occitan (post 1500)',
    'oj' => 'Ojibwa',
    'or' => 'Oriya',
    'om' => 'Oromo',
    'os' => 'Ossetian, Ossetic',
    'pi' => 'Pali',
    'pa' => 'Panjabi, Punjabi',
    'ps' => 'Pashto, Pushto',
    'fa' => 'Persian',
    'pl' => 'Polish',
    'pt' => 'Portuguese',
    'qu' => 'Quechua',
    'rm' => 'Romansh',
    'rn' => 'Rundi',
    'ru' => 'Russian',
    'sm' => 'Samoan',
    'sg' => 'Sango',
    'sa' => 'Sanskrit',
    'sc' => 'Sardinian',
    'sr' => 'Serbian',
    'sn' => 'Shona',
    'sd' => 'Sindhi',
    'si' => 'Sinhala, Sinhalese',
    'sk' => 'Slovak',
    'sl' => 'Slovenian',
    'so' => 'Somali',
    'st' => 'Sotho, Southern',
    'nr' => 'South Ndebele',
    'es' => 'Spanish, Castilian',
    'su' => 'Sundanese',
    'sw' => 'Swahili',
    'ss' => 'Swati',
    'sv' => 'Swedish',
    'tl' => 'Tagalog',
    'ty' => 'Tahitian',
    'tg' => 'Tajik',
    'ta' => 'Tamil',
    'tt' => 'Tatar',
    'te' => 'Telugu',
    'th' => 'Thai',
    'bo' => 'Tibetan',
    'ti' => 'Tigrinya',
    'to' => 'Tonga (Tonga Islands)',
    'ts' => 'Tsonga',
    'tn' => 'Tswana',
    'tr' => 'Turkish',
    'tk' => 'Turkmen',
    'tw' => 'Twi',
    'ug' => 'Uighur, Uyghur',
    'uk' => 'Ukrainian',
    'ur' => 'Urdu',
    'uz' => 'Uzbek',
    've' => 'Venda',
    'vi' => 'Vietnamese',
    'vo' => 'Volap_k',
    'wa' => 'Walloon',
    'cy' => 'Welsh',
    'fy' => 'Western Frisian',
    'wo' => 'Wolof',
    'xh' => 'Xhosa',
    'yi' => 'Yiddish',
    'yo' => 'Yoruba',
    'za' => 'Zhuang, Chuang',
    'zu' => 'Zulu'
];

?><html>
    <head>
        <title><?php echo "Buddha Quotes | BanDev Localisation Project" ?></title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700&display=swap" rel="stylesheet">
        <style>
            .card {
                border-radius: 10px;
            }
            
            .btn {
                border-radius: 10px;
            }
            
            .list-group {
                border-radius: 10px;
            }
            
            .form-control{
                border-radius: 10px;
            }
            
        </style>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-light shadow" style="background-image: url('Picture1.svg'); background-size: 100% 100%;">
            <div class="container">
                <a class="navbar-brand">
                    <div class="media" style="margin-top: 5px;">
                 <img style="margin-right: 10px; " src="buddha.webp" height="50px" width="auto">
  <div class="media-body" >
    <h5 style="font-family: montserrat; font-weight: 700; margin-top: 2px; color:white;">Buddha <br>Quotes</h5>
  </div>
</div>
    
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
     
    </ul>
 <div class="form-inline my-2 my-lg-0" style="background-color: #fff; padding: 10px; border-radius: 11px;">
        
        <div class="media">
  <img src="<?php echo $_SESSION['user']['avatar_url'];?>" height="45px" width="45px" style="border-radius: 50%;" class="mr-3">
  <div class="media-body">
    <a class="mb-0" href="<?php echo $_SESSION['user']['web_url'];?>" style="color: black; font-family: montserrat; font-weight: 500; font-size: 15px; margin-bottom: 0; padding-bottom: 0;"><?php echo $_SESSION['user']['username'];?></a> 
    <br>
    <a style=" font-family: montserrat; font-weight: 500; font-size: 15px; margin-top: -5px; padding-top: -5px;" href="../dashboard/sign_out">Sign Out</a>
  </div>
</div>
    </div>
  </div>
  </div>
</nav>

<div class="container">
    <h3 style="margin-top: 50px; font-family: montserrat; font-weight: 700;">Buddha Quotes <a class="float-right" href="../dashboard" style="font-weight: 500;"><small>Back</small></a></h3>
            <hr>

    <div class="row">
        <div class="col-sm-4">
            <h4 style="font-family: montserrat; font-weight: 700;">Translate</h4>
            <div class="card">
                <div class="card-body">
                    <h5 style="font-family: montserrat; font-weight: 700;">Choose a language</h5>
                    <p style="font-family: montserrat; font-weight: 500;">Pick a language from the dropdown list</p>
                    <form method="get" action="begin">

<select name="lang"style="font-family: montserrat; font-weight: 500;" data-placeholder="Choose a Language..."class="form-control">
  <option value="AF">Afrikaans</option>
  <option value="SQ">Albanian</option>
  <option value="AR">Arabic</option>
  <option value="HY">Armenian</option>
  <option value="EU">Basque</option>
  <option value="BN">Bengali</option>
  <option value="BG">Bulgarian</option>
  <option value="CA">Catalan</option>
  <option value="KM">Cambodian</option>
  <option value="ZH">Chinese (Mandarin)</option>
  <option value="HR">Croatian</option>
  <option value="CS">Czech</option>
  <option value="DA">Danish</option>
  <option value="NL">Dutch</option>
  <option value="ET">Estonian</option>
  <option value="FJ">Fiji</option>
  <option value="FI">Finnish</option>
  <option value="FR">French</option>
  <option value="KA">Georgian</option>
  <option value="DE">German</option>
  <option value="EL">Greek</option>
  <option value="GU">Gujarati</option>
  <option value="HE">Hebrew</option>
  <option value="HI">Hindi</option>
  <option value="HU">Hungarian</option>
  <option value="IS">Icelandic</option>
  <option value="ID">Indonesian</option>
  <option value="GA">Irish</option>
  <option value="IT">Italian</option>
  <option value="JA">Japanese</option>
  <option value="JW">Javanese</option>
  <option value="KO">Korean</option>
  <option value="LA">Latin</option>
  <option value="LV">Latvian</option>
  <option value="LT">Lithuanian</option>
  <option value="MK">Macedonian</option>
  <option value="MS">Malay</option>
  <option value="ML">Malayalam</option>
  <option value="MT">Maltese</option>
  <option value="MI">Maori</option>
  <option value="MR">Marathi</option>
  <option value="MN">Mongolian</option>
  <option value="NE">Nepali</option>
  <option value="NO">Norwegian</option>
  <option value="FA">Persian</option>
  <option value="PL">Polish</option>
  <option value="PT">Portuguese</option>
  <option value="PA">Punjabi</option>
  <option value="QU">Quechua</option>
  <option value="RO">Romanian</option>
  <option value="RU">Russian</option>
  <option value="SM">Samoan</option>
  <option value="SR">Serbian</option>
  <option value="SK">Slovak</option>
  <option value="SL">Slovenian</option>
  <option value="ES">Spanish</option>
  <option value="SW">Swahili</option>
  <option value="SV">Swedish </option>
  <option value="TA">Tamil</option>
  <option value="TT">Tatar</option>
  <option value="TE">Telugu</option>
  <option value="TH">Thai</option>
  <option value="BO">Tibetan</option>
  <option value="TO">Tonga</option>
  <option value="TR">Turkish</option>
  <option value="UK">Ukrainian</option>
  <option value="UR">Urdu</option>
  <option value="UZ">Uzbek</option>
  <option value="VI">Vietnamese</option>
  <option value="CY">Welsh</option>
  <option value="XH">Xhosa</option>
</select>

<button value="submit" class="btn btn-primary btn-block" style="font-family: montserrat; font-weight: 700; margin-top: 20px;">Next</button>

</form>

                </div>
            </div>
             
        </div>
        <div class="col-sm-4">
            <h4 style="font-family: montserrat; font-weight: 700;">Boards</h4>
             <div class="card">
                <div class="card-body">
                    <h5 style='font-family: montserrat; font-weight: 700;'>Priority</h5>
                    <p style='font-family: montserrat; font-weight: 500;'>These languages need to be done soon </p>
                    <div class="list-group">
 
  

                    <?php
       

// Create connection
$conn2 = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$lang_low = $conn2 -> real_escape_string(strtolower($lang_code));

$sql2 = "SELECT * FROM TBL WHERE priority=1";

$result = $conn2->query($sql2);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo '<a href="begin?lang='.strtoupper($row['lang_iso']).'" style="font-family: montserrat; font-weight: 500;" class="list-group-item list-group-item-action">'.$row['lang_str'].'</a>';
  }
} else {
  echo '<a style="font-family: montserrat; font-weight: 500;" class="list-group-item list-group-item-action">0 Languages marked priority</a>';
}

$conn2->close();


        ?>
        </div>

                </div>
            </div>
             <div class="card" style="margin-top: 20px; margin-bottom: 20px;">
                <div class="card-body">
                    <h5 style='font-family: montserrat; font-weight: 700;'>Completed</h5>
                    <p style='font-family: montserrat; font-weight: 500;'>These languages need to be reviewed </p>
                    <div class="list-group">
 
  

                    <?php
        

// Create connection
$conn2 = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$lang_low = $conn2 -> real_escape_string(strtolower($lang_code));

$sql2 = "SELECT * FROM TBL WHERE complete=1 AND ready=0";

$result = $conn2->query($sql2);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo '<a href="review?lang='.strtoupper($row['lang_iso']).'" style="font-family: montserrat; font-weight: 500;" class="list-group-item list-group-item-action">'.$row['lang_str'].'</a>';
  }
} else {
  echo '<a style="font-family: montserrat; font-weight: 500;" class="list-group-item list-group-item-action">0 Languages marked with complete</a>';
}

$conn2->close();


        ?>
        </div>

                </div>
            </div>
            <div class="card" style=" margin-bottom: 20px;">
                <div class="card-body">
                    <h5 style='font-family: montserrat; font-weight: 700;'>Reviewed</h5>
                    <p style='font-family: montserrat; font-weight: 500;'>These languages are waiting for final review & push to app</p>
                    <div class="list-group">
 
  

                    <?php
        

// Create connection
$conn2 = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$lang_low = $conn2 -> real_escape_string(strtolower($lang_code));

$sql2 = "SELECT * FROM TBL WHERE complete=1 AND ready=1";

$result = $conn2->query($sql2);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo '<a href="view?lang='.$row['lang_iso'].'" style="font-family: montserrat; font-weight: 500;" class="list-group-item list-group-item-action">'.$row['lang_str'].'</a>';
  }
} else {
  echo '<a style="font-family: montserrat; font-weight: 500;" class="list-group-item list-group-item-action">0 Languages marked with ready</a>';
}

$conn2->close();


        ?>
        </div>

                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <h4 style="font-family: montserrat; font-weight: 700;">Stats</h4>
<div class="card" style="margin-bottom: 20px;">
    <div class="card-body">
        <?php


// Create connection
$conn2 = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$lang_low = $conn2 -> real_escape_string(strtolower($lang_code));

$sql2 = "SELECT * FROM TBL WHERE current=1";

$result = $conn2->query($sql2);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "<h5 style='font-family: montserrat; font-weight: 700;'>Latest Update: ".$row['updateName']."</h5>";
    echo "<p style='font-family: montserrat; font-weight: 500;'>Current Stings: ".$row['maxStrings']."<p>";
  }
} else {
  echo "0 results";
}

$conn2->close();


        ?>
    </div>
</div>
            
        </div>
    </div>
</div>

    </body>
</html>
