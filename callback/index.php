<?php
session_start();

function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

$code = $_GET['code'];

$state = $_GET['state'];

$APP_ID = "";

$APP_SECRET = "";

$RETURNED_CODE = $code;

$REDIRECT_URI = "";

$Response =  CallAPI("POST", "https://gitlab.com/oauth/token", "client_id=$APP_ID&client_secret=$APP_SECRET&code=$RETURNED_CODE&grant_type=authorization_code&redirect_uri=$REDIRECT_URI");

$resp = json_decode($Response, true);
$token = $resp["access_token"];



$json = file_get_contents("https://gitlab.com/api/v4/user?access_token=$token");

$data = json_decode($json,true);

//var_dump($data);

$uname = $data['username'];

echo "Hello, $uname <br><br>" ;

echo "debug data:<br><br>";
var_dump($data);

$_SESSION['user']['username'] = $uname;

$_SESSION['user']['avatar_url'] = $data['avatar_url'];

$_SESSION['user']['web_url'] = $data['web_url'];



header("location: ../dashboard");
?>

