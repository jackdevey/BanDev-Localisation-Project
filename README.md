<div align="center">
<h1><a href="https://computub.com/bandev/localisation/">BanDev Localisation Project</a></h1>

<img src="images/repo-image.png" height="150" />
</div>

<div align="center">
    <br>
    <strong>Website for localising our apps</strong>
    <br>
    <div align="center">
    </div>
        <br>
        <a href="https://gitlab.com/bandev/buddha-quotes/-/blob/master/LICENSE.md" target="_blank">
        <img src="https://img.shields.io/badge/license-GPL--3.0%2B-informational"/>
        </a>
        </div>

<h2 id="license">License</h2>
<p><a href="http://www.gnu.org/licenses/gpl-3.0.en.html"><img src="https://www.gnu.org/graphics/gplv3-127x51.png" alt="GNU GPLv3 Image"></a>  </p>
<p>The BanDev Localisation Project is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the <a href="https://www.gnu.org/licenses/gpl.html">GNU General Public License</a> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.</p>