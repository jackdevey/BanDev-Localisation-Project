<html lang="en" class="h-100"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>BanDev Localisation Project</title>
    
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700&display=swap" rel="stylesheet">
<script src="https://kit.fontawesome.com/ea68a9ca2c.js" crossorigin="anonymous"></script>    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
    <style>
        body{
            background-image: url("back.jpg");
            background-size: 100%;
            background-repeat: no-repeat;
        }
    </style>

    <!-- Custom styles for this template -->
  </head>
  <body class="d-flex flex-column h-100">
    <!-- Begin page content -->
    <div class="container">
<main role="main" class="flex-shrink-0">
  <div class="container">
    <img style="margin-top: 100px;" src="logo_black_square.webp" height="60px" width="auto">
    <h1 class="mt-1" style="font-family: montserrat; font-weight: 700;">Localisation <br>Project</h1>
    <p class="lead" style="font-family: montserrat; font-weight: 500;">Help translate & localise BanDev's open source<br> applications to help them reach a wider audience across the world.</p>
    <a id="button" href="" class="btn btn-primary" style="font-family: montserrat; font-weight: 700;">Continue with GitLab</a>
<p style="font-family: montserrat; font-weight: 500; margin-top: 10px;"><small>* Anyone can contribute as long as they have <br>a GitLab account.</small></p>
<?php


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM TABLE WHERE active=1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo '<p style="font-family: montserrat; font-weight: 500; margin-top: 10px;"><i class="fas fa-circle" style="color:'.$row['code'].' ;"></i>&nbsp;'.$row['message'].'</p>';
    if($row['code'] == "red"){
        echo "<script>
        document.getElementById('button').href = '#closed';
        document.getElementById('button').innerHTML = 'Continue with GitLab is temporarily disabled';
        document.getElementById('button').classList.add('disabled');
        </script>";
    }
  }
} else {
  echo '<p style="font-family: montserrat; font-weight: 500; margin-top: 10px;">Unknown service status</p>';
}
$conn->close();
?>
  </div>
</main>
</div>
<script>
    function Closed(){
        alert('no');
    }
</script>

</body></html>
