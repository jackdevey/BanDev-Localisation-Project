<?php
session_start();

if(empty($_SESSION['user'])){
    header('location: ../');
}
session_start();
$uname = $_SESSION['user']['username'];

?><html>
    <head>
        <title><?php echo "Hi $uname | BanDev Localisation Project" ?></title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;700&display=swap" rel="stylesheet">

    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-light shadow-sm">
            <div class="container">
                <a class="navbar-brand">
                    <div class="media" style="margin-top: 5px;">
                 <img style="margin-right: 10px; " src="../logo_black_square.webp" height="50px" width="auto">
  <div class="media-body" >
    <h5 style="font-family: montserrat; font-weight: 700; margin-top: 2px;">Localisation <br>Project</h5>
  </div>
</div>
    
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
     
    </ul>
 <div class="form-inline my-2 my-lg-0 border" style="background-color: #fff; padding: 10px; border-radius: 11px;">
        
        <div class="media">
  <img src="<?php echo $_SESSION['user']['avatar_url'];?>" height="45px" width="45px" style="border-radius: 50%;" class="mr-3">
  <div class="media-body">
    <a class="mb-0" href="<?php echo $_SESSION['user']['web_url'];?>" style="color: black; font-family: montserrat; font-weight: 500; font-size: 15px; margin-bottom: 0; padding-bottom: 0;"><?php echo $_SESSION['user']['username'];?></a> 
    <br>
    <a style=" font-family: montserrat; font-weight: 500; font-size: 15px; margin-top: -5px; padding-top: -5px;" href="../dashboard/sign_out">Sign Out</a>
  </div>
</div>
    </div>
  </div>
  </div>
</nav>

<div class="container">
    <h3 style="margin-top: 50px; font-family: montserrat; font-weight: 700;">Dashboard</h3>
            <hr>

    <div class="row">
        <div class="col-sm-6">
            <h4 style="font-family: montserrat; font-weight: 700;">Notices</h4>

    
            <div class="card" style="border-radius: 10px;">
                <div class="card-body">
                    <h5 style="font-family: montserrat; font-weight: 700;">Thank you!</h5>
                    <p style="font-family: montserrat; font-weight: 500;">Thank you for signing up to help improve our apps by translating them into a langauge of your choice! Your user account is not saved to our database and merely retrieves your username for attribution and to prevent spam.</p>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <h4 style="font-family: montserrat; font-weight: 700;">Active Projects</h4>
            <div class="card" style="border-radius: 10px;">
                <div class="card-body" >
                    
                            <div class="media">
  <img src="https://gitlab.com/bandev/buddha-quotes/-/raw/master/assets/buddha.svg" height="100px" width="100px" class="mr-3">
  <div class="media-body">
    <h5  style="font-family: montserrat; font-weight: 700;">Buddha Quotes</h5>
    <p  style="font-family: montserrat; font-weight: 500;">A collaborative project to create a Free and Open Source Buddha Quotes app</p>
    <a  style="font-family: montserrat; font-weight: 700;" class="btn btn-primary" href="../buddha_quotes">Begin</a>
  </div>
</div>

                </div>
            </div>
        </div>
    </div>
</div>

    </body>
</html>